#ifndef D13_TECH_H
#define D13_TECH_H
#include "D13.h"
#include "Requisite.h"
#include <ARKE/Serializable.h>

namespace arke {
    class JSONNode;
}

namespace D13 {
    class Tech : public Serializable {
    public:
        Tech();
        virtual ~Tech();

        std::string id, name;
        std::string description;

        std::string imagePath;

        Requisite requisites;

        std::unordered_map<Tech*, int> dependedOnBy;

        virtual std::string serialize() const override;
        virtual void deserialize(const std::string& str) override;

        virtual void onRegister();
        virtual void onUnregister();

        static std::unordered_map<std::string, Tech*> Index;
        static bool LoadFrom(std::string name, std::string subPath);
        static void LoadAll();
    protected:
        std::vector<std::string> rawDependencies;
    };
}


#endif
