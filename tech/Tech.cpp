#include "Tech.h"
#include <ARKE/JSON.h>
#include <ARKE/AppData.h>
#include <ARKE/Console.h>
#include "Storable.h"

namespace D13 {
    std::unordered_map<std::string, Tech*> Tech::Index;

    Tech::Tech() {

    }
    Tech::~Tech() {

    }

    void Tech::onRegister() {
        Index[this->id] = this;
        //std::cout << "registered " << this->name << " with ID " << this->id <<"\n";
    }
    void Tech::onUnregister() {
        auto it = Index.find(this->id);
        if(it == Index.end()) return;
        Index.erase(it);
    }

    std::string Tech::serialize() const {
        JSON *json = new JSON();

        json->set(this->id, "id");
        json->set(this->name, "name");
        json->set(this->description, "description");

        json->set(this->imagePath, "image");

        this->requisites.serializeTo(json);

        std::string ret = json->serialize();
        delete json;
        return ret;
    }
    void Tech::deserialize(const std::string& str) {
        JSON *json = new JSON();
        json->deserialize(str);

        this->id = json->get<std::string>("id");
        this->name = json->get<std::string>("name");
        this->description = json->get<std::string>("description");

        this->imagePath = json->get<std::string>("image");

        //passes ownership to Requisite
        this->requisites.deserializeFrom(json);

        //delete json;
        //ToLink[&this->requisites] = json;
    }

    bool Tech::LoadFrom(std::string nm, std::string subPath) {
        _unused(subPath);
        unsigned int length = 0;
        char *fdata = AppData::root.readFileText(nm, length);
        if(fdata == nullptr) return false;

        const std::string str = std::string(fdata, length);
        delete [] fdata;

        if(str.size() <= 3) return false;

        Tech *obj = new Tech();
        obj->deserialize(str);
        Console::Out("Loaded tech '", obj->id,"'");
        obj->onRegister();
        return true;
    }

    void Tech::LoadAll() {
        AppData::root.listCallback(&Tech::LoadFrom, true, std::string("tech"));
    }
};
