Format = new function() {};

Format.Short = function(value) {
    var suffix = [' ','k','M','B'];
    var mults = [1,1000,1000000,1000000000];
    var multCount = 4;

    var mult = 0;
    if(value > mults[3]) mult = 3;
    else if(value > mults[2]) mult = 2;
    else if(value > mults[1]) mult = 1;

    if(mult == 0) return value.toString();
    else {
      return (value/mults[mult]) + suffix[mult];
    }
}

Format.Time = function(unixTime) {
    var a = new Date(unixTime * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes() < 10 ? '0' + a.getMinutes() : a.getMinutes();
    var sec = a.getSeconds() < 10 ? '0' + a.getSeconds() : a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
}

Format.DeltaTime = function(unixTime, isShort = false) {
    var dTime = unixTime - Server.CurrentTime();
    var ret = '';
    console.log(dTime);
    var term = ['seconds', 'minutes', 'hours', 'days','weeks','months'];
    var shorts = ['s','m', 'h', 'd', 'w', 'm'];
    var mult = [1,60,3600,86400, 86400*7, 86400*30];
    var minMult = -1;
    var matched = false;

    for(var i = mult.length; i > minMult; i--) {
        if(dTime > mult[i]) {
            ret += Math.floor(dTime / mult[i]) + (isShort? shorts[i] : (' '+term[i])) + ' ';
            console.log(term[i] + ' * ' + Math.floor(dTime/mult[i]));
            dTime -= Math.floor(dTime/mult[i])*mult[i];

            if(!matched) minMult = i > 2? i - 3 : -1;
            matched = true;
            //console.log(minMult);
        }
    }
    return ret.substr(0, ret.length-1);
}

Format.Vertical = function(str) {
    var ret = "";
    for(var i = 0; i < str.length; i++) {
        if(i > 0) ret += "<br />";
        ret += str[i];
    }
    return ret;
}
