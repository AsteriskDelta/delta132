#ifndef D13_D13_INC_H
#define D13_D13_INC_H
#include <ARKE/ARKE.h>
#include <Spinster/Spinster.h>
#include <NVX/NVX.h>

using namespace arke;
namespace arke {
    class HttpReply;
    class HttpRequest;
}

namespace D13 {
    typedef nvx::NI2<48, 16, 0> resi_t;
    typedef nvx::NI2<22, 10, 0> nvxi_t;
    using arke::time_t;

    namespace Galaxy {
        class Galaxy;
        class System;
        class Entity;
        class Node;
        class Orbit;
        class Moon;
        class Planet;
        class Comet;
        class Asteroid;
    };
    namespace Modules {
        class Armor;
        class Bridge;
        class Crypsleep;
        class Fabricator;
        class Habitation;
        class Hull;
        class Medbay;
        class Mine;
        class PointDefense;
        class Shield;
        class Storage;
        class Teleporter;
        class Weapon;
    };
    namespace Admin {
        class Ban;
        class Shadow;
        class Assume;
        class Command;
        class Backup;
        class Crossref;
        class Log;
    };
    //namespace Hubs {
        class Hub;
        class Map;
        class Tile;
    //};
    //namespace Players {
        class Builder;
        class Player;
        class Researcher;
        class Spawn;
        class Language;
    //};
    //namespace Res {
        class Consumable;
        class Food;
        class Requirements;
        class Requisite;
        class Resource;
        class Resources;
        class Storable;
        class StorableIns;
    //};
    namespace Ship {
        class Blueprint;
        class Damage;
        class Module;
        class Ship;
    };
    namespace Social {
        class Censor;
        class Chat;
        class Forum;
    };
    namespace Units {
        class Combatant;
        class Councilor;
        class Officer;
        class Population;
        class Species;
        class Unit;
    };
};

#endif
