NAME = Delta132

#Directory that contains .cpp files with main()
EXEC_DIRS = ./specs/

#directories with classes/files to be compiled into objects
SRC_DIRS =
#shared lib
#client comp shared lib

#directories containing headers that need to be installed
HEADER_DIRS = $(wildcard lib/*) admin client builds controller core design galaxy hub modules npc player resources server ship social tech units
HEADER_CP_DIRS =
HEADER_FILES += D13.h D13Core.h
#Header install subdirectory (ie, in /usr/include: defaults to $(SYS_NAME))
HEADERS_OUT_DIR = Delta132/

#Choose ONE header, if any, to precompile and cache (not for developement!!!)
PCH = D13.h

#Default platform
TARGET_PLATFORM ?= Desktop
#Local build output directory
BUILD_DIR = build

ARKE_LIBS := -Wl,-Bstatic -lboost_system -lboost_filesystem -Wl,-Bdynamic -lGL -lGLEW -lGLU `sdl2-config --libs` -L./dyn/ -lassimp -lfreetype -lnoise -lpthread
ARKE_CFLAGS = `sdl2-config --cflags` `freetype-config --cflags`
LIBS += $(ARKE_LIBS)

ENV_FLAGS +=  -DARKE_VERSION="\"0.0r1-X\"" -DARKE_NAME="\"ARKE\"" -DUSEGL

#Compiler
CXX ?= g++
#CFLAGS (appended to required ones)
CXXPLUS += $(ENV_FLAGS) $(ARKE_CFLAGS)
#SYS_FLAGS (prefix and possible override system CFLAGS, may break things)
CC_SYS_FLAGS ?=
#Optimization flags, supporting PGO if needed
OPTI ?= -O0 -ffast-math
#Include paths, ie -I/path/to/headers/
INCPATH +=
#Libraries, ie -lopenmp
LIBS +=
#Warnings, ie -Wall
WARNINGS ?= -Wall -Wextra -Wnon-virtual-dtor -Wcast-align -Wshadow -Wno-unused-result

ENFORCE_PCH =false
include /usr/include/LPBT.mk

install::

uninstall::

autorun::

disable::
