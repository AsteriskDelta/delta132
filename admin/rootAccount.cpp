#include "D13.h"
#include "Player.h"
#include "Hub.h"
#include "Session.h"
#include <ARKE/Console.h>

namespace D13 {
    Player *RootAccount = nullptr;

    void InitRootAccount() {//WARNING
        //std::cout << "GOT ROOT ACC REQUEST\n";
        Player *player = new Player();
        player->authLevel = 255;
        player->gender = 2;
        player->language = nullptr;
        player->name = "root";
        player->email = "root@localhost";

        player->activeHub = new Hub();
        player->hubs.push_back(player->activeHub);
        player->reindex();
        RootAccount = player;

        Session *fakeSession = new Session("f");
        fakeSession->setPlayer(player, false);
        Session::Index[fakeSession->hash()] = fakeSession;
        Console::Out("Added fake root session with hash()=", fakeSession->hash(), "\n");
    }
}
