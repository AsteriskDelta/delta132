#ifndef D13_RESEARCHER_H
#define D13_RESEARCHER_H
#include "D13.h"

namespace D13 {
    namespace Spawn {
        Player *SpawnPlayer();
        Player *SpawnNPC();
    }
}

#endif
