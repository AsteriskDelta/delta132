#ifndef D13_RESEARCHER_H
#define D13_RESEARCHER_H
#include "D13.h"
#include <ARKE/SmallMap.h>
#include "Research.h"
#include "ResearchIns.h"
#include "ActionQueue.h"
#include "Action.h"

namespace D13 {
    class Tech;
    class ResearchOrder : public Action {
    public:
        ResearchOrder(Player *player, Tech *tech);

        virtual void onCompletion() override;
        virtual void onCancel() override;
    protected:

    };
    class Researcher {
    public:
        //Researcher();
        //virtual ~Researcher();

        ActionQueue<ResearchOrder> researchQueue;
        std::unordered_map<Tech*, int> research;
        //SmallMap<Research*, ResearchIns> research;
    protected:

    };
}

#endif
