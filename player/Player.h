#ifndef D13_PLAYER_H
#define D13_PLAYER_H
#include "D13.h"
#include <ARKE/Deferable.h>
#include "Resources.h"
#include "Researcher.h"
#include "Builder.h"

namespace D13 {
  class Player : public Deferable, public Researcher, public Builder {
  public:
    Player();
    virtual ~Player();

    int8_t authLevel, gender;
    Language *language;
    std::string name, email;

    time_t lastLogin;

    virtual Hub* hub() const;
    std::vector<Hub*> hubs;
    Hub *activeHub;

    virtual void update() override;

    virtual Resources globalResources() const;

    virtual std::string serialize() const override;
    virtual void deserialize(const std::string& str) override;

    virtual void reindex();

    //TODO: LazyIDM
    static std::unordered_map<std::string, Player*> ByName;
    static std::unordered_map<std::string, Player*> ByEmail;
  protected:

  };
};

#endif
