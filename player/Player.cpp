#include "Player.h"
#include "Language.h"
#include "Hub.h"

namespace D13 {
    std::unordered_map<std::string, Player*> Player::ByName;
    std::unordered_map<std::string, Player*> Player::ByEmail;

    Player::Player() {

    }
    Player::~Player() {

    }

    Hub* Player::hub() const {
        return activeHub;
    }

    void Player::update() {
        if(activeHub != nullptr) activeHub->update();
    }

    Resources Player::globalResources() const {

    }

    std::string Player::serialize() const {

    }
    void Player::deserialize(const std::string& str) {

    }

    void Player::reindex() {
        ByName[name] = this;
        ByEmail[email] = this;
    }
}
