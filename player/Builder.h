#ifndef D13_BUILDER_H
#define D13_BUILDER_H
#include "D13.h"
#include "ActionQueue.h"
#include "Action.h"
#include "Map.h"

namespace D13 {
    class Module;
    
    class BuildOrder : public Action {
    public:
        Map *map;
        M2 coords;
        Module *module;

        BuildOrder(Map *m, M2 c, Module *mod, unsigned int cnt);

        virtual void onCompletion() override;
        virtual void onCancel() override;
    protected:

    };
    class Builder {
    public:
        //Builder();
        //virtual ~Builder();

        bool canBuild(M2 c, Module *mod, unsigned int cnt);
        bool build(M2 c, Module *mod, unsigned int cnt);
        bool cancel(unsigned int idx);

        ActionQueue<BuildOrder> buildOrders;
    protected:

    };
}

#endif
