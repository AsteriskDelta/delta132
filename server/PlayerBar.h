#ifndef D13_PLAYER_BAR_H
#define D13_PLAYER_BAR_H
#include "D13.h"

namespace D13 {
    namespace Server {
        HttpReply* PlayerBar(HttpRequest *req);
    }
}

#endif
