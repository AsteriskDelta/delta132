#ifndef ARKE_HTTP_Session_H
#define ARKE_HTTP_Session_H
#include "D13.h"

namespace arke {
    using namespace D13;
  class Session {
  public:
    Session(const std::string& newHash);
    virtual ~Session();

    static constexpr size_t HashLength = 32;
    inline const std::string& hash() const {
        return hash_;
    }

    resi_t timezoneOffset;

    Player* player(bool forceTrue = false) const;
    void setPlayer(Player *newPlayer, bool proxy);

    std::string time() const;

    static Session* Get(const std::string& hash);
    static Session* Create();
    static void Free(Session *session);

    static thread_local Session *Current;
    static std::unordered_map<std::string, Session*> Index;
  protected:
    Player *proxyPlayer, *truePlayer;
    std::string hash_;
  };
};

namespace D13 {
    using Session=arke::Session;
}

#endif
