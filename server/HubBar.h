#ifndef D13_HUB_BAR_H
#define D13_HUB_BAR_H
#include "D13.h"

namespace D13 {
    namespace Server {
        HttpReply* HubBar(HttpRequest *req);
    }
}

#endif
