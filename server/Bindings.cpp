#include "Bindings.h"
#include "HubBar.h"
#include "GalacticMap.h"
#include "HubMap.h"
#include "Login.h"
#include "PlayerBar.h"
#include "Server.h"
#include "Session.h"
#include "TechMap.h"

namespace D13 {
    //std::vector<HttpEndpoint> BoundPaths;
    bool BindServerPaths(HttpServer *server) {
        server->endpoint("/hub_bar", Server::HubBar);
        server->endpoint("/player_bar", Server::PlayerBar);
        return true;
    }
}
