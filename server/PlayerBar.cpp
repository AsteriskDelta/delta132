#include "PlayerBar.h"
#include "Hub.h"
#include "Resource.h"
#include "Player.h"
#include "Server.h"
#include "Session.h"

namespace D13 {
    namespace Server {
        HttpReply* PlayerBar(HttpRequest *req) {
            Player *const player = req->session->player();
            std::cout << "got player at " << player << ", seshID=" << req->session->hash() <<"\n";
            if(!player) return nullptr;
            Hub *const hub = player->hub();
            if(!hub) return nullptr;

            HttpReply *reply = new HttpReply();
            reply->blank();

            reply->set(player->name, "name");
            reply->set(player->email, "email");

            return reply;
        }
    }
}
