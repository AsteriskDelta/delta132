#include "HubBar.h"
#include "Hub.h"
#include "Resource.h"
#include "Player.h"
#include "Server.h"
#include "Session.h"

namespace D13 {
    namespace Server {
        HttpReply* HubBar(HttpRequest *req) {
            Player *const player = req->session->player();
            if(!player) return nullptr;
            Hub *const hub = player->hub();
            if(!hub) return nullptr;

            HttpReply *reply = new HttpReply();
            reply->blank();

            Resources total = hub->storage()->total(),
            storage = hub->storage()->totalMax(),
            delta = hub->producer()->totalProduction();

            unsigned short idx = 0;
            for(auto *const resource : Resource::All) {
                std::string rn = "r"+std::to_string(idx);
                reply->create(rn);
                reply->set(resource->name, rn+".name");
                reply->set(total[resource].str(), rn+".amount");
                reply->set(storage[resource].str(), rn+".storage");
                reply->set(delta[resource].str(), rn+".delta");
                idx++;
            }
            reply->set(std::to_string(idx), std::string("count"));
            return reply;
        }
    }
}
