#ifndef D13_SERV_BINDINGS_H
#define D13_SERV_BINDINGS_H
#include "D13.h"
#include "Server.h"
namespace D13 {
    extern bool BindServerPaths(arke::HttpServer *server);
}

#endif
