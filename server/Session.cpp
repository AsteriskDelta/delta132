#include "Session.h"
#include "Server.h"
#include "Player.h"

namespace arke {
    using namespace D13
    class Player;
    extern Player *RootAccount;
    thread_local Session *Session::Current;
    std::unordered_map<std::string, Session*> Session::Index;

    Session::Session(const std::string& newHash) : proxyPlayer(nullptr), truePlayer(nullptr), hash_(newHash) {

    }
    Session::~Session() {
        //Should only ever be called by Free() below, which unregisters us
    }

    void Session::setPlayer(Player *newPlayer, bool proxy) {
        if(proxy) this->proxyPlayer = newPlayer;
        else this->truePlayer = newPlayer;
    }

    Player* Session::player(bool forceTrue) const {
        if(forceTrue) return truePlayer;
        else if(proxyPlayer != nullptr) return proxyPlayer;
        else return truePlayer;
    }

    std::string Session::time() const {
        return "";
    }

    Session* Session::Get(const std::string& hash) {
        std::cout << "checking hash " << hash << " against:\n";
        for(auto it = Index.begin(); it != Index.end(); ++it) {
            std::cout << "\t" << it->first << " -> " << it->second->player() << ", " << bool(hash == it->first) <<"\n";
        }
        auto it = Index.find(hash);
        if(it == Index.end()) std::cout << "got null\n";
        else std::cout << "got sesh with player@" << it->second->player() << "\n";
        if(it == Index.end()) return nullptr;
        else return it->second;
    }
    Session* Session::Create() {
        std::string nhash;
        nhash.resize(HashLength);
        bool exists = true;
        while(exists) {
            for(uint8_t i = 0; i < HashLength; i++) {
                //!#$%&'()*+-./:<=>?@[]^_`{|}~};
                static constexpr const char lkup[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                uint8_t c = (rand() % (sizeof(lkup)));//Printable ASCII char
                if(i == 0) c = c % 26;//For some gawdawful reason, the HTTP protocol freaks out when a cookie starts with a capital letter.
                nhash[i] = lkup[c];
            }

            if(Get(nhash) == nullptr) exists = false;
        }

        Session *newSession = new Session(nhash);
        Index[nhash] = newSession;
        //newSession->setPlayer(RootAccount, false);
        return newSession;
    }
    void Session::Free(Session *session) {
        auto it = Index.find(session->hash());
        delete session;
        if(it == Index.end()) return;
        else Index.erase(it);
    }
}
