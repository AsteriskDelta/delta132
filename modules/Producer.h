#ifndef D13_PRODUCER_H
#define D13_PRODUCER_H
#include "D13.h"
#include "Resources.h"
#include <ARKE/Deferable.h>

namespace D13 {
    namespace Modules {
        class Producer : public Deferable {
        public:
            Producer();
            virtual ~Producer();

            Resources production;
            Resources totalProduction();

            virtual void refresh() override;
            virtual void update() override;
        protected:

        };
    }
}

#endif
