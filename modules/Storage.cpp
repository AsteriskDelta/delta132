#include "Storage.h"

namespace D13 {
    namespace Modules {
        Storage::Storage() {

        }
        Storage::~Storage() {

        }

        void Storage::add(Resources res, Resources* remainder) {
            Resources del = max - current;
            res.clampTo(del);
            current = current + del;
            res = res - del;

            if(!res.empty()) {
                for(auto it = Proxy::begin<Storage>(); it != Proxy::end<Storage>(); ++it) {
                    it->add(res, &res);
                }
            }

            if(remainder != nullptr) (*remainder) = res;
        }
        bool Storage::has(Resources res) {
            return this->total() >= res;
        }
        bool Storage::take(Resources res, Resources *remainder) {
            if(!this->has(res)) return false;
            else if(res.empty()) return true;

            if(!res.empty()) {
                for(auto it = Proxy::begin<Storage>(); it != Proxy::end<Storage>(); ++it) {
                    it->take(res, &res);
                }
            }

            if(!res.empty()) {
                Resources del = current;
                del.clampTo(res);

                current = current - del;

                res = res - del;
                (*remainder) = res;
            } else (*remainder) = Resources();

            return true;
        }

        Resources Storage::total() {
            Resources ret = current;
            for(auto it = Proxy::begin<Storage>(); it != Proxy::end<Storage>(); ++it) {
                ret = ret + it->total();
            }
            return ret;
        }
        Resources Storage::totalMax() {
            Resources ret = max;
            for(auto it = Proxy::begin<Storage>(); it != Proxy::end<Storage>(); ++it) {
                ret = ret + it->totalMax();
            }
            return ret;
        }
    }
}
