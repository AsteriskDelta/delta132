#include "Module.h"

namespace D13 {
    Module::Module() {

    }
    Module::~Module() {

    }

    void Module::serializeTo(JSONNode *node, const std::string& pth) const {
        _unused(node, pth);
    };
    void Module::deserializeFrom(JSONNode *node, const std::string& pth) {
        _unused(node, pth);
    };

    static void Module::Register(const std::string& id, fn_t function) {
        Index[id] = function;
    }
    static Module* Module::Create(const std::string& id) {
        auto it = Index.find(id);
        if(it == Index.end()) return nullptr;
        else return (*it)();
    }
}
