#ifndef D13_DAMAGABLE_H
#define D13_DAMAGABLE_H
#include "D13.h"
#include "Module.h"
#include "Damage.h"

namespace D13 {
    class Damagable : public Module {
    public:

        Damage current, max;

        virtual void serializeTo(JSONNode *node, const std::string& pth) const override;
        virtual void deserializeFrom(JSONNode *node, const std::string& pth) override;

        static Module* Create();
    protected:

    };
};

#endif
