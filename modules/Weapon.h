#ifndef D13_WEAPON_H
#define D13_WEAPON_H
#include "Module.h"

namespace D13 {
    class Weapon : public Module {
    public:
        struct {
            resi_t avg;
            resi_t stddev;
        } damage;
        resi_t hitsPerTurn;

        Resources costPerFire;
    protected:

    };
}

#endif
