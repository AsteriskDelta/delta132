#include "AllModules.h"

#include "Module.h"
#include "ModulePrototype.h"

#include "Armor.h"
#include "Atmosphere.h"
#include "Bridge.h"
#include "City.h"
//#include "Cryosleep.h"
#include "Damagable.h"
#include "Habitation.h"
#include "Hull.h"
//#include "Medbay.h"
#include "PointDefense.h"
#include "Producer.h"
#include "Shield.h"
#include "Storage.h"
//#include "Teleporter.h"
#include "Weapon.h"

namespace D13 {
    void InitializeModules() {
        using namespace Modules;
        //Module::Register("armor", Armor::Create);
        //Module::Register("atmosphere", Atmosphere::Create);
        //Module::Register("bridge", Bridge::Create);
        //Module::Register("city", City::Create);
        Module::Register("damagable", Damagable::Create);
        Module::Register("habitation", Habitation::Create);
        //Module::Register("hull", Hull::Create);
        //Module::Register("medbay", Medbay::Create);
        //Module::Register("point_defense", PointDefense::Create);
        Module::Register("producer", Producer::Create);
        //Module::Register("shield", Shield::Create);
        Module::Register("storage", Storage::Create);
        //Module::Register("teleporter", Teleporter::Create);
        Module::Register("weapon", Weapon::Create);
    }
}
