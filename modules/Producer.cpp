#include "Producer.h"
#include "Hub.h"
#include "Storage.h"

namespace D13 {
    namespace Modules {
        Producer::Producer() {

        }
        Producer::~Producer() {

        }

        Resources Producer::totalProduction() {
            Resources ret;
            for(auto it = Proxy::begin<Producer>(); it != Proxy::end<Producer>(); ++it) {
                ret = ret + it->totalProduction();
            }
            return ret;
        }

        void Producer::refresh() {
            /*for(auto it = Proxy::begin<Producer>(); it != Proxy::end<Producer>(); ++it) {
                ret = ret + it->total();
            }*/
            Deferable::refresh();
        }
        void Producer::update() {
            this->refresh();
            Hub::Current->storage()->add(production * this->deltaTime());
            Deferable::update();
        }
    }
}
