#ifndef D13_MODULE_H
#define D13_MODULE_H
#include "D13.h"
//#include "Buildable.h"
#include <ARKE/Proxy.h>
#include "Requisite.h"
#include <ARKE/Deferable.h>
#include <ARKE/Serializable.h>
#include <functional>

namespace arke {
    class JSONNode;
}

namespace D13 {
    class Module : public virtual Proxy, public Serializable {
    public:
        Module();
        virtual ~Module();

        //virtual Module* clone();


        virtual void serializeTo(JSONNode *node, const std::string& pth) const override;
        virtual void deserializeFrom(JSONNode *node, const std::string& pth) override;


        typedef std::function<Module*()> fn_t;
        static void Register(const std::string& id, fn_t function);
        static Module* Create(const std::string& id);
    protected:
        std::unordered_map<std::string, fn_t> Index;
    };
}

#endif
