#ifndef D13_MODULE_PROTO_H
#define D13_MODULE_PROTO_H
#include "D13.h"
#include <unordered_map>

namespace D13 {
    class ModuleType;
    class Module;
    class ModulePrototype {
    public:

        std::string id, name;
        std::string description;
        ModuleType *type;

        Requisite requisites;

        std::vector<Module*> modules;

        struct {
            bool occupant;
        };

        Module* createInstance();

        static bool LoadFrom(std::string name, std::string subPath);
        static void LoadAll();

        static std::unordered_map<std::string, ModulePrototype*> Index;
    protected:

    };
}

#endif
