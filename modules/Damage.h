#ifndef D13_DAMAGE_H
#define D13_DAMAGE_H
#include "D13.h"
#include <ARKE/Serializable.h>

namespace arke {
    class JSONNode;
}

namespace D13 {
    class Damage : public Serializable {
    public:
        resi_t hp, armor, shield, energy;
        inline Damage() : hp(0), armor(0), shield(0), energy(0) {};

        virtual void serializeTo(JSONNode *node, const std::string& pth) const override;
        virtual void deserializeFrom(JSONNode *node, const std::string& pth) override;

    protected:
    };
}

#endif
