#ifndef D13_STORAGE_H
#define D13_STORAGE_H
#include "D13.h"
#include "Module.h"
#include "Resources.h"

namespace D13 {
    namespace Modules {
        class Storage : public Module, public virtual Proxy {
        public:
            Storage();
            ~Storage();

            Resources current, max;

            void add(Resources res, Resources* remainder = nullptr);
            bool has(Resources res);
            bool take(Resources res, Resources *remainder = nullptr);

            Resources total();
            Resources totalMax();

        protected:

        };
    };
}

#endif
