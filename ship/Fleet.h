#ifndef D13_FLEET_H
#define D13_FLEET_H
#include "D13.h"
#include "galaxy/GNode.h"
#include "Storage.h"
#include "Producer.h"

namespace D13 {
  class Fleet : public Galaxy::Node, public Modules::Storage, public Modules::Producer {
  public:
    typedef Galaxy::Node Base;

  protected:

  };
};

#endif
