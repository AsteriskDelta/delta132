#include "Storable.h"
#include <ARKE/AppData.h>
#include <ARKE/JSON.h>
#include <functional>
#include <ARKE/Console.h>

namespace D13 {
    std::unordered_map<std::string, Storable*> Storable::Index;

    Storable::Storable() {

    }
    Storable::~Storable() {

    }

    resi_t Storable::mass(resi_t quantity) const {
        return quantity * per.mass;
    }
    resi_t Storable::volume(resi_t quantity) const {
        return quantity * per.volume;
    }
    resi_t Storable::density() const {
        return per.mass/per.volume;
    }

    void Storable::onRegister() {
        Index[this->id] = this;
        //std::cout << "registered " << this->name << " with ID " << this->id <<"\n";
    }
    void Storable::onUnregister() {
        auto it = Index.find(this->id);
        if(it == Index.end()) return;
        Index.erase(it);
    }

    std::string Storable::serialize() const {
        JSON *json = new JSON();

        json->set(this->id, "id");
        json->set(this->name, "name");
        json->set(this->description, "description");
        json->set(const_cast<Storable*>(this)->color.hex(), "color");
        json->set(double(this->per.mass), "mass");
        json->set(double(this->per.volume), "volume");
        json->set(std::string(this->flags.transient? "true":"false"), "transient");

        json->set(this->imagePath, "image");

        std::string ret = json->serialize();
        delete json;
        return ret;
    }
    void Storable::deserialize(const std::string& str) {
        JSON *json = new JSON();
        json->deserialize(str);

        this->id = json->get<std::string>("id");
        this->name = json->get<std::string>("name");
        this->description = json->get<std::string>("description");
        this->color = ColorRGBA(json->get<std::string>("id"));

        this->per.mass = resi_t(json->get<double>("mass"));
        this->per.volume = resi_t(json->get<double>("volume"));

        this->flags.transient = json->get<std::string>("transient") == "true";

        this->imagePath = json->get<std::string>("image");

        delete json;
    }

    bool Storable::LoadFrom(std::string nm, std::string subPath) {
        _unused(subPath);
        //std::cout << "load " << nm << "\n";
        unsigned int length = 0;
        char *fdata = AppData::root.readFileText(nm, length);
        const std::string str = std::string(fdata, length);
        delete [] fdata;

        Storable *storable = new Storable();
        storable->deserialize(str);
        storable->onRegister();
        Console::Out("Loaded storable '", storable->id,"'");
        return false;
    }

    void Storable::LoadAll() {
        AppData::root.listCallback(&Storable::LoadFrom, true, std::string("resources"));
        AppData::root.listCallback(&Storable::LoadFrom, true, std::string("storables"));
    }
}
