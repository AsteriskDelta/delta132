#ifndef D13_REQUISITE_H
#define D13_REQUISITE_H
#include "D13.h"
#include "Resources.h"
#include <unordered_map>
#include <ARKE/Serializable.h>

namespace arke {
    class JSONNode;
}

namespace D13 {
    class Tech;
    class Requisite : public Serializable {
    public:
        Requisite();
        virtual ~Requisite();

        Resources resources;
        std::unordered_map<Tech*, int> techs;
        resi_t completionTime;

        //NOTE: deserializeFrom takes ownership of the JSONNode
        virtual void link(JSONNode *node, const std::string& pth);
        virtual void deserializeFrom(JSONNode *node, const std::string& pth = "requisites") override;
        virtual void serializeTo(JSONNode *node, const std::string& pth = "requisites") const override;

        static void LinkAll();
    protected:
        static std::unordered_map<Requisite*,JSONNode*> ToLink;
        static std::unordered_map<Requisite*,std::string> ToLinkPaths;
    };
}

#endif
