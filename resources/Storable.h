#ifndef D13_INC_STORABLE_H
#define D13_INC_STORABLE_H
#include "D13.h"
#include <ARKE/Serializable.h>
#include <ARKE/Color.h>

namespace D13 {
    class Storable : public Serializable {
    public:
        Storable();
        virtual ~Storable();

        std::string id;
        std::string name, shortName;
        std::string description;

        ColorRGBA color;

        std::string imagePath;

        resi_t mass(resi_t quantity) const;
        resi_t volume(resi_t quantity) const;
        resi_t density() const;

        virtual void onRegister();
        virtual void onUnregister();

        virtual std::string serialize() const override;
        virtual void deserialize(const std::string& str) override;

        static std::unordered_map<std::string, Storable*> Index;
        static bool LoadFrom(std::string name, std::string subPath);
        static void LoadAll();
    protected:
        struct {
            resi_t mass;
            resi_t volume;
        } per;
        struct {
            bool transient : 1;
        } flags;

        std::string loadPath;
    };
};

#endif
