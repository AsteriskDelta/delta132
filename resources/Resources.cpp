#include "Resources.h"

namespace D13 {
    resi_t Resources::mass() const {
        resi_t ret = 0;
        for(const auto& ins : instances) {
            ret += ins.mass();
        }
        return ret;
    }
    resi_t Resources::volume() const {
        resi_t ret = 0;
        for(const auto& ins : instances) {
            ret += ins.volume();
        }
        return ret;
    }

    resi_t& Resources::operator[](Storable *idx) {
        for(auto& ins : instances) {
            if(ins.prototype == idx) return ins.quantity;
        }
        static resi_t zero = 0;
        return zero;
    }
    const resi_t& Resources::operator[](Storable *idx) const {
        for(const auto& ins : instances) {
            if(ins.prototype == idx) return ins.quantity;
        }
        static resi_t zero = 0;
        return zero;
    }

    Resources Resources::operator*(resi_t mult) const {
        Resources ret;
        for(const auto& ins : instances) {
            ret.add(ins.prototype, ins.quantity*mult);
        }
        return ret;
    }
    Resources Resources::operator-(const Resources& o) const {
        Resources ret;
        for(const auto& ins : instances) {
            Storable *proto = ins.prototype;
            resi_t quantity = ins.quantity;
            for(const auto& oins : o.instances) {
                if(oins.prototype == proto) quantity -= ins.quantity;
            }
            ret.add(proto, quantity);
        }
        return ret;
    }
    Resources Resources::operator+(const Resources& o) const {
        Resources ret = o;
        for(const auto& ins : instances) {
            Storable *proto = ins.prototype;
            resi_t quantity = ins.quantity;
            ret.add(proto, quantity);
        }
        return ret;
    }

    bool Resources::operator>=(const Resources& o) const {
        for(const auto& ins : instances) {
            Storable *proto = ins.prototype;
            resi_t quantity = ins.quantity;
            for(const auto& oins : o.instances) {
                if(oins.prototype == proto) {
                    if(quantity < oins.quantity) return false;
                }
            }
        }
        return true;
    }

    void Resources::add(Storable *tproto, resi_t quantity) {
        for(auto& ins : instances) {
            Storable *proto = ins.prototype;
            if(proto == tproto) {
                ins.quantity += quantity;
                return;
            }
        }

        this->instances.push_back(StorableIns(tproto, quantity));
    }

    Resources Resources::clampTo(const Resources& o) const {
        Resources ret;
        for(const auto& ins : instances) {
            Storable *proto = ins.prototype;
            resi_t quantity = ins.quantity;
            for(const auto& oins : o.instances) {
                if(oins.prototype == proto) quantity = min(quantity, ins.quantity);
            }
            ret.add(proto, quantity);
        }
        return ret;
    }

    bool Resources::empty() const {
        return instances.empty();
    }
}
