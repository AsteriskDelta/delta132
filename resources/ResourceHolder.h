#ifndef D13_RESOURCE_HOLDER_H
#define D13_RESOURCE_HOLDER_H
#include "D13.h"
#include "Resource.h"

namespace D13 {
    class ResourceHolder {
    public:
        Researcher();
        virtual ~Researcher();

        Resources resources;
    protected:

    };
}

#endif
