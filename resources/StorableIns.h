#ifndef D13_INC_STORABLE_INS_H
#define D13_INC_STORABLE_INS_H
#include "D13.h"

namespace D13 {
    class StorableIns {
    public:
        StorableIns(Storable *proto = nullptr, resi_t quant = 0);

        Storable *prototype;
        resi_t quantity;

        resi_t mass() const;
        resi_t volume() const;

    protected:

    };
};

#endif
