#include "Requisite.h"
#include "Storable.h"
#include "StorableIns.h"
#include "Tech.h"
#include <ARKE/JSON.h>
#include <ARKE/Console.h>

namespace D13 {
    std::unordered_map<Requisite*, JSONNode*> Requisite::ToLink;
    std::unordered_map<Requisite*, std::string> Requisite::ToLinkPaths;

    Requisite::Requisite() {

    }
    Requisite::~Requisite() {

    }

    void Requisite::link(JSONNode *node, const std::string& pth) {
        for(JSONNode sub : node->getNode(pth+".techs")) {
            int val = sub.get<double>();
            const std::string& rawProto = sub.path;
            Tech *proto = Tech::Index[rawProto];
            if(proto != nullptr) this->techs[proto] = val;
            else {
                Console::Err("Could not find technology '",rawProto,"'");
            }
        }
    }

    void Requisite::deserializeFrom(JSONNode *node, const std::string& pth) {
        this->completionTime = resi_t(node->get<double>(pth+".time"));

        for(JSONNode sub : node->getNode(pth+".cost")) {
            resi_t val = sub.get<double>();
            const std::string& rawProto = sub.path;
            Storable *proto = Storable::Index[rawProto];
            if(proto != nullptr) this->resources.instances.push_back(StorableIns(proto, val));
            else {
                Console::Err("Could not find storable type '",rawProto,"'");
            }
        }

        ToLink[this] = node;
        ToLinkPaths[this] = pth;
    }
    void Requisite::serializeTo(JSONNode *node, const std::string& pth) const {
        node->create(pth+".techs");
        for(const auto& it : this->techs) {
            node->set(double(it.second), pth+".techs."+it.first->id);
        }
        node->create(pth+".cost");
        for(const StorableIns& ins : resources.instances) {
            node->set(double(ins.quantity), pth+".cost."+ins.prototype->id);
        }
        node->set(double(this->completionTime), pth+".time");
    }

    void Requisite::LinkAll() {
        for(auto it : ToLink) {
            it.first->link(it.second, ToLinkPaths[it.first]);
            delete it.second;
        }
        ToLink.clear();
        ToLinkPaths.clear();
    }
}
