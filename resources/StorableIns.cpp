#include "StorableIns.h"
#include "Storable.h"

namespace D13 {
    StorableIns::StorableIns(Storable *proto, resi_t quant) : prototype(proto), quantity(quant) {

    }
    resi_t StorableIns::mass() const {
        return prototype->mass(this->quantity);
    }
    resi_t StorableIns::volume() const {
        return prototype->volume(this->quantity);
    }
}
