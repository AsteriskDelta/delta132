#ifndef D13_INC_RESOURCES_H
#define D13_INC_RESOURCES_H
#include "D13.h"
#include <vector>
#include "StorableIns.h"

namespace D13 {
    class Resources {
    public:
        //Resources();
        //~Resources();

        std::vector<StorableIns> instances;

        resi_t mass() const;
        resi_t volume() const;

        void add(Storable *proto, resi_t quantity);

        resi_t& operator[](Storable *idx);
        const resi_t& operator[](Storable *idx) const;

        Resources operator*(resi_t mult) const;
        //Resources operator*(time_t time) const;
        Resources operator-(const Resources& o) const;
        Resources operator+(const Resources& o) const;

        bool operator>=(const Resources& o) const;

        Resources clampTo(const Resources& o) const;

        bool empty() const;
    protected:

    };
};

#endif
