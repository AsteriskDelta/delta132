#ifndef D13_RESOURCE_H
#define D13_RESOURCE_H
#include "D13.h"
#include "Storable.h"

namespace D13 {
    class Resource : public Storable {
    public:

        virtual bool load(const std::string& path);

        virtual void onRegister() override;
        virtual void onUnregister() override;

        static std::unordered_set<Resource*> All;
        static void LoadAll();
    protected:

    };
}

#endif
