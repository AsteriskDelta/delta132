#ifndef D13_RESEARCHER_H
#define D13_RESEARCHER_H
#include "D13.h"
#include "player/Player.h"
#include "Disposition.h"
#include "Trait.h"

namespace D13 {
    class Disposition {
    public:
        Disposition();
        virtual ~Disposition();

    protected:

    };
}

#endif
