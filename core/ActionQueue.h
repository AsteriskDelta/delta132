#ifndef D13_ACTIONQUEUE_H
#define D13_ACTIONQUEUE_H
#include "D13.h"
#include <vector>
#include <ARKE/Deferable.h>
#include "Action.h"

namespace D13 {
    class Action;

    template<typename T>
    class ActionQueue : public Deferable {
    public:
        typedef T Type;
        inline ActionQueue() {

        }
        inline virtual ~ActionQueue() {

        }

        std::vector<Type*> actions;

        inline virtual void update() override {

        }
    protected:

    };
};

#endif
