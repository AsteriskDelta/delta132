#ifndef D13_ACTION_H
#define D13_ACTION_H
#include "D13.h"
#include <ARKE/Deferable.h>

namespace D13 {
  class Action : public Deferable {
  public:
    Action();
    virtual ~Action();

    virtual void onCompletion();
    virtual void onCancel();

    struct {
        resi_t current, max;
        inline resi_t percent() const {
            return current / max * resi_t(100);
        }
    } progress;

    struct {
        unsigned int current, total;
    } count;

    inline resi_t percent() const {
        return (progress.percent() + resi_t(count.current * 100))
                / (resi_t(count.total) * progress.max);
    }

    virtual bool done() const;

    virtual void update() override;
  protected:

  };
};

#endif
