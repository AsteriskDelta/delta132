#include "Hub.h"

namespace D13 {
    Modules::Storage* Hub::storage() {
        return this;
    }
    Modules::Producer* Hub::producer() {
        return this;
    }

    thread_local Hub* Hub::Current = nullptr;
}
