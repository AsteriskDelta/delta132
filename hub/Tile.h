#ifndef D13_TILE_H
#define D13_TILE_H
#include "D13.h"

namespace D13 {
    class Biome;
    class Tile {
    public:
        nvxi_t temperature;
        nvxi_t elevation;
        nvxi_t moisture;

        Biome *biome;

        nvxi_t population;
        nvxi_t infrastructure;

        Resources *naturalResources;
    protected:

    };
}

#endif
