#ifndef D13_HUB_H
#define D13_HUB_H
#include "D13.h"
#include "Map.h"
#include "Module.h"
#include "Storage.h"
#include "Producer.h"

namespace D13 {
    class Hub : public Map, public Modules::Producer, public Modules::Storage {
    public:

        std::vector<Module*> modules;

        Modules::Storage* storage();
        Modules::Producer *producer();

        static thread_local Hub* Current;
    protected:

    };
}

#endif
