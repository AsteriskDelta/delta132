#ifndef D13_MAP_H
#define D13_MAP_H
#include "D13.h"
#include "Tile.h"

namespace D13 {
    typedef N2T<nvxi_t> M2;

    class Map {
    public:
        M2 scale;
        std::vector<std::vector<Tile>> tiles;
    protected:

    };
}

#endif
