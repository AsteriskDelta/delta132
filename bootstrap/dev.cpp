#include "D13.h"
#include "D13Core.h"
#include <ARKE/Console.h>
#include <ARKE/Application.h>
#include "Server.h"
#include "Time.h"
#include "Bindings.h"
#include "rootAccount.cpp"

using namespace D13;
int background(int status);

int main(int argc, char** argv) {
    _unused(argc, argv);

    Application::BeginInitialization();
    Application::SetName("Sector 132");
    Application::SetCodename("com.d3.sector132");
    Application::SetCompanyName("Delta III Technologies");
    Application::SetVersion("0.0r1-X");
    Application::SetDataDir("../../data");
    Application::EndInitialization();

    const unsigned short lclPort = 2941;
    Spin::MemberInfo myInfo;
    myInfo.type = Application::GetCodename();
    Spin::MemberID myID(5, lclPort);

    unsigned short prt = 1384;//1001+rand()%400;
    Console::Out("Starting on port ", prt);

    HttpServer *server = new HttpServer();
    server->initialize(prt, 8);
    server->endpoint("/test", [](HttpRequest *req) -> HttpReply* {
        delete req;
        HttpReply *ret = new HttpReply();
        ret->set(std::string("it works"), "text");
        return ret;
    });
    BindServerPaths(server);

    Console::Out("Starting Spinster...");
    Spin::Initialize(myID, myInfo);

    Spin::Local->schedule(Spin::Priority::Realtime, background, 0);

    Console::Out("Preparing Server...");
    server->prepare();

    return 0;
}

int background(int status) {
    Console::Out("Paralell BG task created");
    Console::Out("Initializing root account");
    InitRootAccount();
    
    Storable::LoadAll();
    Tech::LoadAll();

    Requisite::LinkAll();

    Console::Out("Init completed");

    while(true) {
        Time::Current = time(nullptr);
        usleep(1000);
    }
    return status;
}
//*/
/*
#include <pistache/http.h>
#include <pistache/router.h>
#include <pistache/endpoint.h>

using namespace std;
using namespace Pistache;

void printCookies(const Http::Request& req) {
    auto cookies = req.cookies();
    std::cout << "Cookies: [" << std::endl;
    const std::string indent(4, ' ');
    for (const auto& c: cookies) {
        std::cout << indent << c.name << " = " << c.value << std::endl;
    }
    std::cout << "]" << std::endl;
}

namespace Generic {

void handleReady(const Rest::Request&, Http::ResponseWriter response) {
    response.send(Http::Code::Ok, "1");
}

}

class StatsEndpoint {
public:
    StatsEndpoint(Address addr)
        : httpEndpoint(std::make_shared<Http::Endpoint>(addr))
    { }

    void init(size_t thr = 2) {
        auto opts = Http::Endpoint::options()
            .threads(thr)
            .flags(Tcp::Options::InstallSignalHandler);
        httpEndpoint->init(opts);
        setupRoutes();
    }

    void start() {
        httpEndpoint->setHandler(router.handler());
        httpEndpoint->serve();
    }

    void shutdown() {
        httpEndpoint->shutdown();
    }

private:
    void setupRoutes() {
        using namespace Rest;

        Routes::Post(router, "/record/:name/:value?", Routes::bind(&StatsEndpoint::doRecordMetric, this));
        Routes::Get(router, "/value/:name", Routes::bind(&StatsEndpoint::doGetMetric, this));
        Routes::Get(router, "/ready", Routes::bind(&Generic::handleReady));
        Routes::Get(router, "/auth", Routes::bind(&StatsEndpoint::doAuth, this));

    }

    void doRecordMetric(const Rest::Request& request, Http::ResponseWriter response) {
        auto name = request.param(":name").as<std::string>();

        Guard guard(metricsLock);
        auto it = std::find_if(metrics.begin(), metrics.end(), [&](const Metric& metric) {
            return metric.name() == name;
        });

        int val = 1;
        if (request.hasParam(":value")) {
            auto value = request.param(":value");
            val = value.as<int>();
        }

        if (it == std::end(metrics)) {
            metrics.push_back(Metric(std::move(name), val));
            response.send(Http::Code::Created, std::to_string(val));
        }
        else {
            auto &metric = *it;
            metric.incr(val);
            response.send(Http::Code::Ok, std::to_string(metric.value()));
        }

    }

    void doGetMetric(const Rest::Request& request, Http::ResponseWriter response) {
        auto name = request.param(":name").as<std::string>();

        Guard guard(metricsLock);
        auto it = std::find_if(metrics.begin(), metrics.end(), [&](const Metric& metric) {
            return metric.name() == name;
        });

        if (it == std::end(metrics)) {
            response.send(Http::Code::Not_Found, "Metric does not exist");
        } else {
            const auto& metric = *it;
            response.send(Http::Code::Ok, std::to_string(metric.value()));
        }

    }

    void doAuth(const Rest::Request& request, Http::ResponseWriter response) {
        printCookies(request);
        response.cookies()
            .add(Http::Cookie("lang", "en-US"));
        response.send(Http::Code::Ok);
    }

    class Metric {
    public:
        Metric(std::string name, int initialValue = 1)
            : name_(std::move(name))
            , value_(initialValue)
        { }

        int incr(int n = 1) {
            int old = value_;
            value_ += n;
            return old;
        }

        int value() const {
            return value_;
        }

        std::string name() const {
            return name_;
        }
    private:
        std::string name_;
        int value_;
    };

    typedef std::mutex Lock;
    typedef std::lock_guard<Lock> Guard;
    Lock metricsLock;
    std::vector<Metric> metrics;

    std::shared_ptr<Http::Endpoint> httpEndpoint;
    Rest::Router router;
};

int main(int argc, char *argv[]) {
    Port port(9080);

    int thr = 2;

    if (argc >= 2) {
        port = std::stol(argv[1]);

        if (argc == 3)
            thr = std::stol(argv[2]);
    }

    Address addr(Ipv4::any(), port);

    cout << "Cores = " << hardware_concurrency() << endl;
    cout << "Using " << thr << " threads" << endl;

    StatsEndpoint stats(addr);

    stats.init(thr);
    stats.start();

    stats.shutdown();
}
//*/
