#ifndef D13_GENTITY_H
#define D13_GENTITY_H
#include <ARKE/Proxy.h>

namespace D13 {
    namespace Galaxy {
        class Entity : public virtual Proxy {
        public:
            Entity();
            virtual ~Entity();
        protected:

        };
    }
}

#endif
