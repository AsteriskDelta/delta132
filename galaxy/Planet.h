#ifndef D13_PLANET_H
#define D13_PLANET_H
#include "D13.h"
#include <ARKE/SmallMap.h>
#include "SmallSet.h"
#include "Orbit.h"

namespace D13 {
    namespace Galaxy {
        class Planet : public Orbit {
        public:
            Planet();
            virtual ~Planet();

            std::string name;

        protected:

        };
    };
}

#endif
