#ifndef D13_STAR_H
#define D13_STAR_H
#include "D13.h"
#include <ARKE/SmallMap.h>

namespace D13 {
    namespace Galaxy {
        class Star {
        public:
            Star();
            virtual ~Star();

        protected:

        };
    };
}

#endif
