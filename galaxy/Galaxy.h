#ifndef D13_GALAXY_H
#define D13_GALAXY_H
#include "D13.h"
#include "GEntity.h"

namespace D13 {
  namespace Galaxy {
    class Galaxy : public Entity {
    public:
        Galaxy();
        ~Galaxy();
    protected:

    };
  };
};


#endif
