#ifndef D13_GNODE_H
#define D13_GNODE_H
#include "D13.h"
#include "GEntity.h"

namespace D13 {
  namespace Galaxy {
    class Node : public Entity {
    public:
        Node();
        virtual ~Node();

    protected:
    };
  };
};


#endif
