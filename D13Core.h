#ifndef D13_CORE_INC_H
#define D13_CORE_INC_H

#include "builds/Building.h"
#include "builds/Constructable.h"

#include <ARKE/Server.h>

#include "galaxy/Asteroids.h"
#include "galaxy/Comet.h"
#include "galaxy/Galaxy.h"
#include "galaxy/GEntity.h"
#include "galaxy/GNode.h"
#include "galaxy/GSystem.h"
#include "galaxy/Moon.h"
#include "galaxy/Orbit.h"
#include "galaxy/Planet.h"

#include "hub/Hub.h"
#include "hub/Map.h"
#include "hub/Tile.h"

#include "player/Builder.h"
#include "player/Player.h"
#include "player/Researcher.h"
#include "player/Spawn.h"

#include "resources/Consumable.h"
//#include "resources/Food.h"
#include "resources/Requisite.h"
#include "resources/Resource.h"
#include "resources/Resources.h"

#include "ship/Blueprint.h"
#include "ship/Damage.h"
#include "ship/Module.h"
#include "ship/Ship.h"

#include "social/Censor.h"
#include "social/Chat.h"
#include "social/Forum.h"

#include "units/Combatant.h"
#include "units/Councilor.h"
#include "units/Officer.h"
#include "units/Species.h"
#include "units/Unit.h"
#include "units/Population.h"

#include "tech/Tech.h"

#endif
